#Package: Web Pattern

**Introduction**

The web pattern takes express, Handlebars, 0Auth and New Relic and organizes them into a ready to use website project. It's a fast way to get working on your Node website without having to worry about the setup. It takes care of all the boring parts, like setting up your views, 404 page, 500 page, getting the modules organized, etc...

Web Patternhas following goals:

    1.  Using NodeJS Express Web Server and return the app when initialized.
    2.  Enabling Gzip compression on server to improve bandwidth efficiency to client
    3.  Referenced as a package in the "Web" base repository
    4.  Handling Web Server Logging setup
    5. Handling Web Server Sessions
    6. Including view engine (Handlebars)
    7. Normalize package preconfigured as a dependency and used by Express base app.
    8. Web Auth package preconfigured as a dependency and used by Express base app.

The module uses the following external libraries

    1.  Express
    2.  Morgan
    3.  Consolidate
    4.  Compression
    5.  Sitemap
    
**Sample Usage**

Use the following syntax to use the module in your node Application

        'use strict';

        const option = {
            basePath:__dirname
        }

        const app = require('express-boilerplate')(option);
        //app variable initialized and ready for use

Available options parameters

        const option = {
            basePath: __dirname // required. used to get current application base path
            publicFolder: 'public' // optional (default 'public')
            viewsFolder: 'views' // optional (default 'views')
            routesFolder: 'routes' // optional (default 'routes')
            sitemap: '/sitemap.xml' // optional (default '/sitemap.xml')
        }

**Environment Variables**

Auth0 and Passport require several several environment configuration variables to be able to work properly. Here is the list of all environment variables used the the middleware. In some cases, where feasible, the middleware assumes some defaults.

*Please note the the suffix after Auth is numerical zero (0) and not alphabetical O*

The Auth0 configuration options will be available via the Administrator. The following values needs to be set.
Usually the Auth0 will be consistent for all the applications.
* AUTH0_CLIENT_ID
* AUTH0_CLIENT_SECRET
* AUTH0_CONNECTION
* AUTH0_DOMAIN

![Auth0 Configuration](http://res.cloudinary.com/xenotime/image/upload/v1482148053/Screen_Shot_2016-12-19_at_5.15.57_PM_hpexio.png)


Other Per Application Environment Variable Configuration
* ALLOW_ANONYMOUS (Allowed Values true/false, Default : false)
* HOST (No default)
* CALLBACK_URL (default /callback)
* ERROR_PAGE (default /error)
* SESSION_SECRET (default value present)

Notes
*The middleware searches for Allow Anonymous environment variable. If the variable is missing, it authenticates all requests, including css and images etc.
The Host value should be set to current Heroku host name. Auth0 will redirect back using the hostname and the callback url.*

#### Auth0 Configuration
1. Session Timeout
Session timeout is set to 1800 seconds i.e. 30 minutes, via the Auth0 Client.
2. An AddOn Saml2 WebApp has been added on Auth0 client to for bypassing the Auth0 screen
![Saml2 AddOn](http://res.cloudinary.com/dkzejh7qv/image/upload/v1477564042/Saml.png)


**Running Tests on Middleware**

`npm run test`

**Running Lint on Middleware**

`npm run lint`
