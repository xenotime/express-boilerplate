/**
 * Created by xenotime on 03/11/16.
 */
const sm = require('sitemap');

module.exports = (app, objs, hostName) =>
  (req, res) => {
    const datetime = new Date();
    const urls = [];

    // loop through the routes objects to build out a list of urls
    objs.forEach((i) => {
      i.forEach((j) => {
        if (j.route && j.route.path) {
          urls.push({
            url: j.route.path,
            changefreq: 'monthly',
            priority: 0.5,
            lastmodrealtime: false,
            lastmod: datetime,
          });
        }
      });
    });

    // generate sitemap
    const sitemap = sm.createSitemap({
      hostname: hostName,
      cacheTime: 600000,
      urls,
    });

    // display the sitemap
    sitemap.toXML((err, xml) => {
      if (err) {
        res.status(500).end(JSON.stringify(err));
      } else {
        res.header('Content-Type', 'application/xml');
        res.send(xml);
      }
    });
  };
