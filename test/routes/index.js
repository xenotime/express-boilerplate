
module.exports = (routes) => {
  routes.get('/longdata', (req, res) => {
    var body = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sed lacus quis massa malesuada\
        rhoncus et ut lectus. Donec id pulvinar elit. Donec bibendum purus ac nisi eleifend luctus. Mauris\
        sit amet dolor congue diam porttitor tincidunt id in dolor. Etiam sit amet nulla ac quam auctor ultrices\
        et in augue. Proin imperdiet elit vitae enim viverra iaculis. Mauris nisi magna, ultricies quis erat\
        sodales, fringilla fermentum massa. Mauris orci dolor, vestibulum eget congue sit amet, mattis eu ante.\
        Mauris vitae mattis leo. Sed volutpat erat non sollicitudin luctus. Nam dapibus iaculis velit, eu sodales\
        lorem vestibulum commodo. Vestibulum lacinia commodo enim, vitae fermentum ex blandit eget. Lorem ipsum \
        dolor sit amet, consectetur adipiscing elit. In sed lacus quis massa malesuada\
        rhoncus et ut lectus. Donec id pulvinar elit. Donec bibendum purus ac nisi eleifend luctus. Mauris\
        sit amet dolor congue diam porttitor tincidunt id in dolor. Etiam sit amet nulla ac quam auctor ultrices\
        et in augue. Proin imperdiet elit vitae enim viverra iaculis. Mauris nisi magna, ultricies quis erat\
        sodales, fringilla fermentum massa. Mauris orci dolor, vestibulum eget congue sit amet, mattis eu ante.\
        Mauris vitae mattis leo. Sed volutpat erat non sollicitudin luctus. Nam dapibus iaculis velit, eu sodales\
        lorem vestibulum commodo. Vestibulum lacinia commodo enim, vitae fermentum ex blandit eget.\
        dolor sit amet, consectetur adipiscing elit. In sed lacus quis massa malesuada\
        rhoncus et ut lectus. Donec id pulvinar elit. Donec bibendum purus ac nisi eleifend luctus. Mauris\
        sit amet dolor congue diam porttitor tincidunt id in dolor. Etiam sit amet nulla ac quam auctor ultrices\
        et in augue. Proin imperdiet elit vitae enim viverra iaculis. Mauris nisi magna, ultricies quis erat\
        sodales, fringilla fermentum massa. Mauris orci dolor, vestibulum eget congue sit amet, mattis eu ante.\
        Mauris vitae mattis leo. Sed volutpat erat non sollicitudin luctus. Nam dapibus iaculis velit, eu sodales\
        lorem vestibulum commodo. Vestibulum lacinia commodo enim, vitae fermentum ex blandit eget.";

    res.setHeader('Content-Type', 'text/plain');
    res.send(body);
  });

  routes.get('/', (req, res) => {
    res.send("Response");
  });
};
