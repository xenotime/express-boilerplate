/**
 * Created by xenotime on 03/11/16.
 */

process.env.ENVIRONMENT = 'dev1';
process.env.PORT='3000';
process.env.ALLOW_ANONYMOUS='true';
process.env.AUTH0_DOMAIN='app.auth0.com';
process.env.AUTH0_CLIENT_ID='<client id>';
process.env.AUTH0_CLIENT_SECRET='<slient secret>';
process.env.AUTH0_CONNECTION='Web-Auth';
process.env.HOST='http://localhost:3000';
process.env.CALLBACK_URL='/callback';
process.env.ERROR_PAGE='/error';
process.env.SESSION_SECRET='sssshhhhh';

var assert = require('chai').assert;
var express = require('express');
var request = require('supertest');
var app = require('../index')({
  basePath:__dirname,
  FORCEHTTPS:false,
  KEEPWWW: true
});

describe('Web Pattern', function () {

  it('should handle bad routes', function (done) {

    request(app)
      .get('/notvalidurl')
      .expect(404)
      .end(function (err, res) {
        if (err) return done(err);
        done();
      });
  });

  it('should set a templating engine, currently handlebars only', function(done){

    var templates = ['hbs'];	// Add more engines if needed

    assert.include(templates,app["settings"]["view engine"],'uses handlebars as templating engine');
    done();

  });

  it('correctly compresses responses with gzip compression', function (done) {

    request(app)
      .get('/longdata')
      .set('X-Forwarded-Proto', 'https')
      .set('Accept-Encoding', 'gzip')
      .expect('Content-Encoding', 'gzip')
      .expect('vary', 'Accept-Encoding')
      .end(function(err, res) {
        if (err) return done(err);
        done();
      });

  });

});


// This will create a virtual hostname that unit tests can leverage when needed
function createVhostTester(app, vhost) {
  const real = request(app);
  const proxy = {};
  Object.keys(real).forEach(methodName => {
    proxy[methodName] = function() {
      return real[methodName]
        .apply(real, arguments)
        .set('host', vhost);
    }
  });

  return proxy;
}