/**
 * Created by xenotime on 03/11/16.
 */

// Environment variables are stored as strings only, we need boolean values
// allowing user the flexibilty to enter
// yes, True etc

const fs = require('fs');
const express = require('express');
const compression = require('compression');
const path = require('path');
const logger = require('morgan');
const cons = require('consolidate');
const auth = require('./lib/auth');
const mySiteMap = require('./lib/sitemap');

module.exports = (options) => {
  const localOptions = {
    useAuth: options.useAuth || false,
    basePath: options.basePath,
    publicFolder: options.publicFolder || 'public',
    viewsFolder: options.viewsFolder || 'views',
    routesFolder: options.routesFolder || 'routes',
    sitemap: options.sitemap || '/sitemap.xml',
    ERROR_PAGE: process.env.ERROR_PAGE || '/error',
  };

  const app = express();

  app.use(compression({
    filter: (req, res) => {
      if (req.headers['x-no-compression']) {
        // don't compress responses with this request header
        return false;
      }

      // fallback to standard filter function
      return compression.filter(req, res);
    },
  }));

  // Setup Views
  app.set('views', path.join(localOptions.basePath, localOptions.viewsFolder));
  app.engine('hbs', cons.handlebars);
  app.set('view engine', 'hbs');

  app.use(express.static(path.join(localOptions.basePath, localOptions.publicFolder)));

  // Middleware for Universal Redirection and Response configuration
  app.use((req, res, next) => { // eslint-disable-line consistent-return
    'use strict'; // eslint-disable-line strict, lines-around-directive

    let urlProtocol = req.protocol;
    let urlHost = (process.env.HOST === undefined ? req.headers.host : process.env.HOST);
    let urlPath = req.originalUrl.split('?')[0];
    const urlQueryString = (req.originalUrl.split('?')[1] === undefined ? '' : `?${req.originalUrl.split('?')[1]}`);
    let redirectRequired = false;

    // Force HTTPS
    const hasHttps = (req.headers['x-forwarded-proto'] === 'https' || urlProtocol === 'https');
    if (!hasHttps && localOptions.FORCEHTTPS) {
      redirectRequired = true;
      urlProtocol = 'https';
    }

    // Force Lowercase Path
    if (/[A-Z]/.test(urlPath)) {
      redirectRequired = true;
      urlPath = urlPath.toLowerCase();
    }

    // Force Trailing Slash Removal
    if (urlPath.substr(-1) === '/' && urlPath.length > 1) {
      redirectRequired = true;
      urlPath = urlPath.slice(0, -1);
    }

    // Force WWW Removal
    if (urlHost.indexOf('www.') === 0 && !localOptions.KEEPWWW) {
      redirectRequired = true;
      urlHost = urlHost.slice('www.'.length);
    }

    // Redirect if Needed
    if (redirectRequired) {
      res.redirect(301, `${urlProtocol}://${urlHost}${urlPath}${urlQueryString}`);
    } else {
      return next();
    }
  });

  // sets lillyAuth middleware
  if (localOptions.useAuth) {
    app.use(auth(app, {
      AUTH0_CLIENT_ID: process.env.AUTH0_CLIENT_ID,
      AUTH0_CLIENT_SECRET: process.env.AUTH0_CLIENT_SECRET,
      AUTH0_CONNECTION: process.env.AUTH0_CONNECTION,
      AUTH0_DOMAIN: process.env.AUTH0_DOMAIN,
    }));
  }

  // Load all routes
  const routes = express.Router(); // eslint-disable-line new-cap
  try {
    fs.readdirSync(`${localOptions.basePath}/${localOptions.routesFolder}`).forEach((file) => {
      if (file.substr(-3) === '.js') {
        // eslint-disable-next-line import/no-dynamic-require, global-require
        const router = require(`${localOptions.basePath}/${localOptions.routesFolder}/${file}`);
        router(routes);
      }
    });
    app.use('/', routes);
  } catch (ex) {
    console.log(ex.message);
  }

  // sitemap generation
  // what routes should we look inside to generate the sitemap.xml?
  const objs = [app._router.stack, routes.stack]; // eslint-disable-line no-underscore-dangle
  app.use(localOptions.sitemap, mySiteMap(app, objs, process.env.HOST));

  // You can set morgan to log differently depending on your environment
  if (process.env.ENVIRONMENT === 'dev') {
    app.use(logger('dev'));
  } else {
    app.use(logger('common', {
      skip: (req, res) => res.statusCode < 400,
      stream: fs.createWriteStream(`${localOptions.basePath}/morgan.log`, { flags: 'a' }),
    }));
  }

  // catch 404 and forward to error handler
  // Error Handling Routes
  app.use((req, res) => {
    res.status(404).render(localOptions.ERROR_PAGE.substring(1), {
      message: '404 Page not found!',
    });
  });

  // error handlers

  // development error handler
  // will print stacktrace
  if (process.env.ENVIRONMENT === 'dev') {
    app.use((err, req, res, next) => { // eslint-disable-line no-unused-vars
      res.status(err.status || 500);
      res.render(localOptions.ERROR_PAGE.substring(1), {
        message: err.message,
        error: err,
      });
    });
  }

  // production error handler
  // no stacktraces leaked to user
  app.use((err, req, res, next) => { // eslint-disable-line no-unused-vars
    res.status(err.status || 500);
    res.render(localOptions.ERROR_PAGE.substring(1), {
      message: err.message,
      error: {},
    });
  });

  return app;
};
